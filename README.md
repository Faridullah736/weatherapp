## Overview
 Using [Openweathermap API](https://openweathermap.org/api), build with SwiftUI and Combine. 
 Feature current weather & details, location mapview, hourly & daliy forecast.

### Keyword
- SwiftUI & Combine
- JSON & Codable
- MVVM state management 
- ObservableObject
- Modules: Data, Domain, Common and Application

## Getting Started

### Installation

1. Clone or download the project to your local machine
2. Pod Install to create workplace, no external dependencies.
3. Application starting with mock data coordinates.

### Search
- city name with enter/done button action.

Run the simulator
