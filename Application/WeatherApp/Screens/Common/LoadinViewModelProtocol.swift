

import Foundation

protocol LoadingViewModelProtocol: AnyObject {
    var isAnimating: Bool { get set }
}

extension LoadingViewModelProtocol {
    func startLoading() {
        isAnimating = true
    }

    func stopLoading() {
        isAnimating = false
    }
}
