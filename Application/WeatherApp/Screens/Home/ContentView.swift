//
//  ContentView.swift
//  WeatherApp
//
//  Created by Farid Ullah on 07/09/2023.
//

import SwiftUI
import Common

struct ContentView: View {
    @StateObject var homeViewModel = HomeViewModel()
    @State private var search: String = ""
    
    var body: some View {
        LoadingView(isShowing: $homeViewModel.isAnimating) {
            ZStack(alignment: .top) {
                VStack {
                    TextField("Search City", text: $search).onSubmit {
                        homeViewModel.getNewCity(name: search)
                        search = ""
                    }
                    .multilineTextAlignment(TextAlignment.center)
                    if homeViewModel.stateView  == .success {
                        LocationAndTemperatureHeaderView(data: homeViewModel.currentWeather)
                        Spacer()

                        ScrollView(.vertical, showsIndicators: false) {
                            VStack {
                                DailyWeatherCellView(data: homeViewModel.todayWeather)
                                Rectangle().frame(height: CGFloat(1))

                                HourlyWeatherView(data: homeViewModel.hourlyWeathers)
                                Rectangle().frame(height: CGFloat(1))

                                DailyWeatherView(data: homeViewModel.dailyWeathers)
                                Rectangle().frame(height: CGFloat(1))

                                DetailsCurrentWeatherView(data: homeViewModel.currentWeather)
                                Rectangle().frame(height: CGFloat(1))
                            }
                            Spacer()
                            MapView(coord: homeViewModel.mapCoordinate)
                                .frame(height: 200)

                        }
                        Spacer()


                    }

                    if homeViewModel.stateView == .failed {
                        Button(action: {
                            mockCityData()
                        }) {
                            Text("Failed get data, retry?")
                                .foregroundColor(.white)
                        }
                    }
                }
            }
        }.onAppear(perform: {
            mockCityData()
        })
        .background(Color.black)
        .edgesIgnoringSafeArea(.bottom)
        .preferredColorScheme(.dark)
    }

    private func mockCityData() {
        homeViewModel.getCurrentWeather(lat: 38.7223,
                                       lon: -9.142685)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
