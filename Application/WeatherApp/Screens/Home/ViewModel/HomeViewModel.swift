
import Foundation
import Common
import Domain
import Data

class HomeViewModel: ObservableObject, LoadingViewModelProtocol {
    
    @Published var isAnimating: Bool = false
    @Published var stateView: StateView = .loading
    @Published var currentWeather: CurrentWeather = .emptyInit()

    @Published var todayWeather:ForecastWeather = ForecastWeather.emptyInit()
    @Published var hourlyWeathers: [ForecastWeather] = []
    @Published var dailyWeathers: [ForecastWeather] = []
    @Published var mapCoordinate: Coordinate = Coordinate.emptyInit()

    private let weatherManager: WeatherDataManagerProtocol

    private var stateCurrentWeather = StateView.loading
    private var stateForecastWeather = StateView.loading

    init(weatherManager: WeatherDataManagerProtocol = WeatherDataManager()) {
        self.weatherManager = weatherManager
    }

    func getNewCity(name: String) {
        startLoading()
        weatherManager.getCityWeather(param: name)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self else { return }
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("API Call Error: \(error)")
                    self.stateView = .failed
                }
            }, receiveValue: { [weak self] responseModel in
                guard let self,
                      let lat = responseModel.first?.lat,
                      let lon = responseModel.first?.lon else {return}
                self.stopLoading()
                self.getCurrentWeather(lat: lat, lon: lon)
            }).cancel()
    }

    func getCurrentWeather(lat: Double, lon: Double) {
        startLoading()
        weatherManager.getCurrentWeather(lat: lat, lon: lon)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self else { return }
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("API Call Error: \(error)")
                    self.stateView = .failed
                }
            }, receiveValue: { [weak self] responseModel in
                guard let self else { return }
                self.currentWeather = responseModel
                self.stateView = .success
                self.todayWeather = currentWeather.getForecastWeather()
                self.mapCoordinate = currentWeather.coordinate
                self.updateStateView()
                self.stopLoading()
            }).cancel()
        getForecastWeather(lat: lat, lon: lon)
    }

    private func getForecastWeather(lat: Double, lon: Double) {
        startLoading()
        weatherManager.getForecastWeather(lat: lat, lon: lon)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                guard let self else { return }
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("API Call Error: \(error)")
                    self.stateView = .failed
                }
            }, receiveValue: { [weak self] responseModel in
                guard let self else { return }
                self.hourlyWeathers = responseModel.list
                self.dailyWeathers = responseModel.dailyList
                self.stateForecastWeather = .success
                self.updateStateView()
                self.stopLoading()
            }).cancel()
    }

    private func updateStateView() {
        if stateCurrentWeather == .success, stateForecastWeather == .success {
            stateView = .success
        }

        if stateCurrentWeather == .failed, stateForecastWeather == .failed {
            stateView = .failed
        }
    }
    
    private func sortByAMPM(_ time1: String, _ time2: String) -> Bool {
        let isAM1 = time1.contains("AM")
        let isAM2 = time2.contains("AM")

        if isAM1 == isAM2 {
            return false
        } else {
            return isAM1
        }
    }

}

