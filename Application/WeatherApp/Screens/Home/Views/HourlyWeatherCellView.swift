
import SwiftUI
import Domain

struct HourlyWeatherCellView: View {
    var data: ForecastWeather
    private var hour: String {
        return data.date.dateFromMilliseconds().hour()
    }

    private var temperature: String {
        return "\(Int(data.mainValue.temp))°"
    }
    
    private var icon: String {
        var image = "WeatherIcon"
        if let weather = data.elements.first {
            image = weather.icon
        }
        return image
    }

    var body: some View {
        VStack {
            Text(hour)
            Text("\(data.mainValue.humidity)%")
                .font(.system(size: 12))
                .foregroundColor(
                    .init(red: 127/255,
                          green: 1,
                          blue: 212/255)
                )
            AsyncImage(url: URL(string: icon.iconUrl))
                .frame(width: 30, height: 30)
            
            Text(temperature)
        }.padding(.all, 0)
    }
}

struct HourlyWeatherCellView_Previews: PreviewProvider {
    static var previews: some View {
        HourlyWeatherCellView(data: ForecastWeather.emptyInit())
    }
}
