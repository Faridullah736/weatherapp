
import SwiftUI
import Domain

struct LocationAndTemperatureHeaderView: View {
    let data: CurrentWeather
    
   private var temperature: String {
        return "\(Int(data.mainValue.temp))°"
    }
    
   private var weatherName: String {
        var result = ""
        if let weather = data.elements.first {
            result = weather.main
        }
        return result
    }
    
    var body: some View {
        VStack {
            Text("Today,\(Date().formatted(.dateTime.month().day().hour().minute()))")
                                    .fontWeight(.light)
            Text(data.name)
                .font(.largeTitle)
                .fontWeight(.medium)
            Text(weatherName)
                .font(.body)
                .fontWeight(.light)
                .padding(.bottom, 4)
            Text(temperature)
                .font(.system(size: 40))
                .fontWeight(.thin)
        }
        .padding(.vertical, 24)
    }
}
