
import SwiftUI
import MapKit
import Common

struct MapView: View {
    let coord: Coordinate
    @State private var mapID = UUID()
    
    private var coordinateRegion: MKCoordinateRegion {
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: coord.lat, longitude: coord.lon),
            span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        )
    }
    var body: some View {
        Map(coordinateRegion: .constant(coordinateRegion))
            .padding()
            .cornerRadius(40, corners: [.allCorners])
            .id(mapID)


    }
}

