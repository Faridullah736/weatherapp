

import Foundation

public struct Coordinate: Codable {
   public let lon, lat: Double
    
   public static func emptyInit() -> Coordinate {
        return Coordinate(lon: 0, lat: 0)
    }
}
