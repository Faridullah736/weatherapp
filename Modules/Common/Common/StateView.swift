
import Foundation

public enum StateView {
    case loading
    case success
    case failed
}
