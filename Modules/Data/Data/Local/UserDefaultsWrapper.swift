
import Domain

public struct UserDefaultsWrapper {
    @Storage(key: "current_weather_key", defaultValue: CurrentWeather.emptyInit())
    public static var currentWeatherStoreData: CurrentWeather

    @Storage(key: "forecast_weather_key", defaultValue: ForecastWeatherResponse.emptyInit())
    public static var forecastWeatherStoreData: ForecastWeatherResponse
}
