
import Foundation
import Network

enum WeatherAPI {
    case getForecastWeather(lat: Double, lon: Double)
    case getCurrentWeather(lat: Double, lon: Double)
    case getCities(search: String)
}

extension WeatherAPI: NetworkEndpoint {

    var apiKey: String {
        "1e4589f7b9111747e783bc7134d33ddf"
    }
    var baseURL: URL {
        return URL(string: "https://api.openweathermap.org/")!
    }

    var path: String {
        switch self {
        case .getCities:
            return "geo/1.0/direct"
        case .getCurrentWeather:
            return "data/2.5/weather"
        case .getForecastWeather:
            return "data/2.5/forecast"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getCities, .getCurrentWeather, .getForecastWeather:
            return .get
        }
    }
    var parameters: [String : Any]? {
        switch self {
        case .getCities(let param):
            return ["q": param, "appid": apiKey]
        case .getCurrentWeather(let lat, let lon), .getForecastWeather(let lat, let lon):
            return ["lat": lat, "lon": lon, "units": "metric", "appid": apiKey]
        }
    }

}
