
import Foundation
import Domain

public extension WeatherDataManager {
    convenience init() {
        self.init(remoteDataSource: WeatherRemoteDataSource(),
                  localRepository: WeatherLocalDataSource())
    }
}
