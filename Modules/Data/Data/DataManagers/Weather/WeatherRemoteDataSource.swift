
import Foundation
import Domain
import Combine
import Network

struct WeatherRemoteDataSource: WeatherRemoteRepository {
    private let networkManager = URLSessionAPIClient<WeatherAPI>()

    init() {}

    func getCurrentWeather(lat: Double, lon: Double) -> AnyPublisher<CurrentWeather, Error>  {
        networkManager.request(WeatherAPI.getCurrentWeather(lat: lat, lon: lon)).eraseToAnyPublisher()
    }

    func getForecastWeather(lat: Double, lon: Double) -> AnyPublisher<ForecastWeatherResponse, Error>  {
        networkManager.request(WeatherAPI.getForecastWeather(lat: lat, lon: lon)).eraseToAnyPublisher()
    }

    func getCityWeather(param: String) -> AnyPublisher<[CitySearch], Error> {
        networkManager.request(WeatherAPI.getCities(search: param)).eraseToAnyPublisher()
    }

}

