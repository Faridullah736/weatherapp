

import Foundation
import Domain

public struct WeatherLocalDataSource: WeatherLocalRepository {


    init() {}
    public var currentWeather: CurrentWeather {
        get {
            UserDefaultsWrapper.currentWeatherStoreData
        }
        set {
            UserDefaultsWrapper.currentWeatherStoreData = newValue
        }
    }

    public var forecastWeather: ForecastWeatherResponse {
        get {
            UserDefaultsWrapper.forecastWeatherStoreData
        }
        set {
            UserDefaultsWrapper.forecastWeatherStoreData = newValue
        }
    }
}
