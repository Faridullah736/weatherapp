

import Foundation

public protocol NetworkEndpoint {
    var baseURL: URL { get }
    var path: String { get }
    var apiKey: String { get }
    var method: HTTPMethod { get }
    var headers: [String: String]? { get }
    var parameters: [String: Any]? { get }
}

public extension NetworkEndpoint {
    var headers: [String: String]? {
        return nil
    }

    var parameters: [String: Any]? {
        return nil
    }
}


public enum NetworkError: Error {
    case requestFailed
    case responseUnsuccessful(statusCode: Int)
    case invalidData
    case jsonParsingFailure
    case invalidURL
}
