
import Foundation
import Combine

public class URLSessionAPIClient<EndpointType: NetworkEndpoint>: NetworkClient {
    public init() {}

    public func request<T: Decodable>(_ endpoint: EndpointType) -> AnyPublisher<T, Error>  {
        let url = endpoint.baseURL.appendingPathComponent(endpoint.path)
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.rawValue

        // Set up any request headers or parameters here
        endpoint.headers?.forEach { request.addValue($0.value, forHTTPHeaderField: $0.key) }
        if let parameters = endpoint.parameters {
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
            components?.queryItems = parameters.map { key, value in
                URLQueryItem(name: key, value: String(describing: value))
            }
            if let finalURL = components?.url {
                request.url = finalURL
            }
        }

        return URLSession.shared.dataTaskPublisher(for: request)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw NetworkError.invalidData
                }

                guard (200...299).contains(httpResponse.statusCode) else {
                    throw NetworkError.responseUnsuccessful(statusCode:httpResponse.statusCode)
                }
                return data
            }
            .flatMap { data -> AnyPublisher<T, Error> in
                do {
                    let decodedData = try JSONDecoder().decode(T.self, from: data)
                    return Just(decodedData)
                        .setFailureType(to: Error.self)
                        .eraseToAnyPublisher()
                } catch {
                    return Fail(error: error).eraseToAnyPublisher()
                }
            }
            .subscribe(on: DispatchQueue.global(qos: .background))
            .eraseToAnyPublisher()
    }

}
