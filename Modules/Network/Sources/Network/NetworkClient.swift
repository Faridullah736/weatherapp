

import Foundation
import Combine

public protocol NetworkClient {
    associatedtype EndpointType: NetworkEndpoint
    func request<T: Decodable>(_ endpoint: EndpointType) -> AnyPublisher<T, Error> 
}
