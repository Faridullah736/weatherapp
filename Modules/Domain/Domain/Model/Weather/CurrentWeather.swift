import Foundation
import Common

public struct CurrentWeather: Codable {
    public let timezone, id: Int
    public let name: String
    public let coordinate: Coordinate
    public let elements: [WeatherElement]
    public let base: String
    public let mainValue: CurrentWeatherMainValue
    public let visibility: Int
    public let wind: WeatherWind
    public let clouds: WeatherClouds
    public let date: Int
    public let sys: CurrentWeatherSys
    public let code: Int
    
    enum CodingKeys: String, CodingKey {
        case base, visibility, wind, clouds, sys, timezone, id, name
        case elements = "weather"
        case coordinate = "coord"
        case mainValue = "main"
        case date = "dt"
        case code = "cod"
    }
    
    public static func emptyInit() -> CurrentWeather {
        return CurrentWeather(
            timezone: 0,
            id: 0,
            name: "",
            coordinate: Coordinate.emptyInit(),
            elements: [],
            base: "",
            mainValue: CurrentWeatherMainValue.emptyInit(),
            visibility: 0,
            wind: WeatherWind.emptyInit(),
            clouds: WeatherClouds.emptyInit(),
            date: 0,
            sys: CurrentWeatherSys.emptyInit(),
            code: 0
        )
    }
    
   public func description() -> String {
        var result = "Today: "
        if let weatherElement = elements.first {
            result += "\(weatherElement.weatherDescription.capitalizingFirstLetter()) currently. "
        }
        result += "It's \(mainValue.temp)°."
        return result
    }
    
   public func getForecastWeather() -> ForecastWeather {
        var result = ForecastWeather.emptyInit()

        result.date = self.date
        result.mainValue.tempMin = self.mainValue.tempMin
        result.mainValue.tempMax = self.mainValue.tempMax

        if let weatherElement = elements.first {
            result.elements.append(weatherElement)
        }

        return result
    }
}
