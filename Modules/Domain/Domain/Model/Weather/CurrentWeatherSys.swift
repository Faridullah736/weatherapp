import Foundation

public struct CurrentWeatherSys: Codable {
    public let type, id: Int
    public let country: String
    public let sunrise, sunset: Int
    
   public static func emptyInit() -> CurrentWeatherSys {
        return CurrentWeatherSys(
            type: 0,
            id: 0,
            country: "",
            sunrise: 0,
            sunset: 0
        )
    }
}
