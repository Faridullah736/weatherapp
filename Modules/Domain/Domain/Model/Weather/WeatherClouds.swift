import Foundation

public struct WeatherClouds: Codable {
    public let all: Int

    public static func emptyInit() -> WeatherClouds {
        return WeatherClouds(all: 0)
    }
}
