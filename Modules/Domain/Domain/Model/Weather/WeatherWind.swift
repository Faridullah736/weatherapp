import Foundation

public struct WeatherWind: Codable {
    public let speed: Double
    public let deg: Int?
    
    public static func emptyInit() -> WeatherWind {
        return WeatherWind(speed: 0.0, deg: nil)
    }
}
