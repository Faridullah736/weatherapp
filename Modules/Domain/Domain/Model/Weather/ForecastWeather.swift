import Foundation

public struct ForecastWeather: Codable {
   public var date: Int
   public var mainValue: ForecastWeatherMainValue
   public var elements: [WeatherElement]
   public let clouds: WeatherClouds
   public let wind: WeatherWind

    enum CodingKeys: String, CodingKey {
        case clouds, wind
        case mainValue = "main"
        case date = "dt"
        case elements = "weather"
    }
    
   public static func emptyInit() -> ForecastWeather {
        return ForecastWeather(
            date: 0,
            mainValue: ForecastWeatherMainValue.emptyInit(),
            elements: [],
            clouds: WeatherClouds.emptyInit(),
            wind: WeatherWind.emptyInit()
        )
    }
}

extension ForecastWeather: Identifiable {
   public var id: String { "\(date)" }
}
