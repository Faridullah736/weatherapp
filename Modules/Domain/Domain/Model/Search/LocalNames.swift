import Foundation

public struct LocalNames : Codable {
   public let ms : String?
   public let gu : String?
   public let isName : String?
   public let wa : String?
   public let mg : String?
   public let gl : String?
   public let om : String?
   public let ku : String?
   public let tw : String?
   public let mk : String?
   public let ee : String?
   public let fj : String?
   public let gd : String?
   public let ky : String?
   public let yo : String?
   public let zu : String?
   public let bg : String?
   public let tk : String?
   public let co : String?
   public let sh : String?
   public let de : String?
   public let kl : String?
   public let bi : String?
   public let km : String?
   public let lt : String?
   public let fi : String?
   public let fy : String?
   public let ba : String?
   public let sc : String?
   public let feature_name : String?
   public let ja : String?
   public let am : String?
   public let sk : String?
   public let mr : String?
   public let es : String?
   public let sq : String?
   public let te : String?
   public let br : String?
   public let uz : String?
   public let da : String?
   public let sw : String?
   public let fa : String?
   public let sr : String?
   public let cu : String?
   public let ln : String?
   public let na : String?
   public let wo : String?
   public let ig : String?
   public let to : String?
   public let ta : String?
   public let mt : String?
   public let ar : String?
   public let su : String?
   public let ab : String?
   public let ps : String?
   public let bm : String?
   public let mi : String?
   public let kn : String?
   public let kv : String?
   public let os : String?
   public let bn : String?
   public let li : String?
   public let vi : String?
   public let zh : String?
   public let eo : String?
   public let ha : String?
   public let tt : String?
   public let lb : String?
   public let ce : String?
   public let hu : String?
   public let it : String?
   public let tl : String?
   public let pl : String?
   public let sm : String?
   public let en : String?
   public let vo : String?
   public let el : String?
   public let sn : String?
   public let fr : String?
   public let cs : String?
   public let io : String?
   public let hi : String?
   public let et : String?
   public let pa : String?
   public let av : String?
   public let ko : String?
   public let bh : String?
   public let yi : String?
   public let sa : String?
   public let sl : String?
   public let hr : String?
   public let si : String?
   public let so : String?
   public let gn : String?
   public let ay : String?
   public let se : String?
   public let sd : String?
   public let af : String?
   public let ga : String?
   public let or : String?
   public let ia : String?
   public let ie : String?
   public let ug : String?
   public let nl : String?
   public let gv : String?
   public let qu : String?
   public let be : String?
   public let an : String?
   public let fo : String?
   public let hy : String?
   public let nv : String?
   public let bo : String?
   public let ascii : String?
   public let id : String?
   public let lv : String?
   public let ca : String?
   public let no : String?
   public let nn : String?
   public let ml : String?
   public let my : String?
   public let ne : String?
   public let he : String?
   public let cy : String?
   public let lo : String?
   public let jv : String?
   public let sv : String?
   public let mn : String?
   public let tg : String?
   public let kw : String?
   public let cv : String?
   public let az : String?
   public let oc : String?
   public let th : String?
   public let ru : String?
   public let ny : String?
   public let bs : String?
   public let st : String?
   public let ro : String?
   public let rm : String?
   public let ff : String?
   public let kk : String?
   public let uk : String?
   public let pt : String?
   public let tr : String?
   public let eu : String?
   public let ht : String?
   public let ka : String?
   public let ur : String?

    enum CodingKeys: String, CodingKey {

        case ms = "ms"
        case gu = "gu"
        case isName = "is"
        case wa = "wa"
        case mg = "mg"
        case gl = "gl"
        case om = "om"
        case ku = "ku"
        case tw = "tw"
        case mk = "mk"
        case ee = "ee"
        case fj = "fj"
        case gd = "gd"
        case ky = "ky"
        case yo = "yo"
        case zu = "zu"
        case bg = "bg"
        case tk = "tk"
        case co = "co"
        case sh = "sh"
        case de = "de"
        case kl = "kl"
        case bi = "bi"
        case km = "km"
        case lt = "lt"
        case fi = "fi"
        case fy = "fy"
        case ba = "ba"
        case sc = "sc"
        case feature_name = "feature_name"
        case ja = "ja"
        case am = "am"
        case sk = "sk"
        case mr = "mr"
        case es = "es"
        case sq = "sq"
        case te = "te"
        case br = "br"
        case uz = "uz"
        case da = "da"
        case sw = "sw"
        case fa = "fa"
        case sr = "sr"
        case cu = "cu"
        case ln = "ln"
        case na = "na"
        case wo = "wo"
        case ig = "ig"
        case to = "to"
        case ta = "ta"
        case mt = "mt"
        case ar = "ar"
        case su = "su"
        case ab = "ab"
        case ps = "ps"
        case bm = "bm"
        case mi = "mi"
        case kn = "kn"
        case kv = "kv"
        case os = "os"
        case bn = "bn"
        case li = "li"
        case vi = "vi"
        case zh = "zh"
        case eo = "eo"
        case ha = "ha"
        case tt = "tt"
        case lb = "lb"
        case ce = "ce"
        case hu = "hu"
        case it = "it"
        case tl = "tl"
        case pl = "pl"
        case sm = "sm"
        case en = "en"
        case vo = "vo"
        case el = "el"
        case sn = "sn"
        case fr = "fr"
        case cs = "cs"
        case io = "io"
        case hi = "hi"
        case et = "et"
        case pa = "pa"
        case av = "av"
        case ko = "ko"
        case bh = "bh"
        case yi = "yi"
        case sa = "sa"
        case sl = "sl"
        case hr = "hr"
        case si = "si"
        case so = "so"
        case gn = "gn"
        case ay = "ay"
        case se = "se"
        case sd = "sd"
        case af = "af"
        case ga = "ga"
        case or = "or"
        case ia = "ia"
        case ie = "ie"
        case ug = "ug"
        case nl = "nl"
        case gv = "gv"
        case qu = "qu"
        case be = "be"
        case an = "an"
        case fo = "fo"
        case hy = "hy"
        case nv = "nv"
        case bo = "bo"
        case ascii = "ascii"
        case id = "id"
        case lv = "lv"
        case ca = "ca"
        case no = "no"
        case nn = "nn"
        case ml = "ml"
        case my = "my"
        case ne = "ne"
        case he = "he"
        case cy = "cy"
        case lo = "lo"
        case jv = "jv"
        case sv = "sv"
        case mn = "mn"
        case tg = "tg"
        case kw = "kw"
        case cv = "cv"
        case az = "az"
        case oc = "oc"
        case th = "th"
        case ru = "ru"
        case ny = "ny"
        case bs = "bs"
        case st = "st"
        case ro = "ro"
        case rm = "rm"
        case ff = "ff"
        case kk = "kk"
        case uk = "uk"
        case pt = "pt"
        case tr = "tr"
        case eu = "eu"
        case ht = "ht"
        case ka = "ka"
        case ur = "ur"
    }

   public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        ms = try values.decodeIfPresent(String.self, forKey: .ms)
        gu = try values.decodeIfPresent(String.self, forKey: .gu)
        isName = try values.decodeIfPresent(String.self, forKey: .isName)
        wa = try values.decodeIfPresent(String.self, forKey: .wa)
        mg = try values.decodeIfPresent(String.self, forKey: .mg)
        gl = try values.decodeIfPresent(String.self, forKey: .gl)
        om = try values.decodeIfPresent(String.self, forKey: .om)
        ku = try values.decodeIfPresent(String.self, forKey: .ku)
        tw = try values.decodeIfPresent(String.self, forKey: .tw)
        mk = try values.decodeIfPresent(String.self, forKey: .mk)
        ee = try values.decodeIfPresent(String.self, forKey: .ee)
        fj = try values.decodeIfPresent(String.self, forKey: .fj)
        gd = try values.decodeIfPresent(String.self, forKey: .gd)
        ky = try values.decodeIfPresent(String.self, forKey: .ky)
        yo = try values.decodeIfPresent(String.self, forKey: .yo)
        zu = try values.decodeIfPresent(String.self, forKey: .zu)
        bg = try values.decodeIfPresent(String.self, forKey: .bg)
        tk = try values.decodeIfPresent(String.self, forKey: .tk)
        co = try values.decodeIfPresent(String.self, forKey: .co)
        sh = try values.decodeIfPresent(String.self, forKey: .sh)
        de = try values.decodeIfPresent(String.self, forKey: .de)
        kl = try values.decodeIfPresent(String.self, forKey: .kl)
        bi = try values.decodeIfPresent(String.self, forKey: .bi)
        km = try values.decodeIfPresent(String.self, forKey: .km)
        lt = try values.decodeIfPresent(String.self, forKey: .lt)
        fi = try values.decodeIfPresent(String.self, forKey: .fi)
        fy = try values.decodeIfPresent(String.self, forKey: .fy)
        ba = try values.decodeIfPresent(String.self, forKey: .ba)
        sc = try values.decodeIfPresent(String.self, forKey: .sc)
        feature_name = try values.decodeIfPresent(String.self, forKey: .feature_name)
        ja = try values.decodeIfPresent(String.self, forKey: .ja)
        am = try values.decodeIfPresent(String.self, forKey: .am)
        sk = try values.decodeIfPresent(String.self, forKey: .sk)
        mr = try values.decodeIfPresent(String.self, forKey: .mr)
        es = try values.decodeIfPresent(String.self, forKey: .es)
        sq = try values.decodeIfPresent(String.self, forKey: .sq)
        te = try values.decodeIfPresent(String.self, forKey: .te)
        br = try values.decodeIfPresent(String.self, forKey: .br)
        uz = try values.decodeIfPresent(String.self, forKey: .uz)
        da = try values.decodeIfPresent(String.self, forKey: .da)
        sw = try values.decodeIfPresent(String.self, forKey: .sw)
        fa = try values.decodeIfPresent(String.self, forKey: .fa)
        sr = try values.decodeIfPresent(String.self, forKey: .sr)
        cu = try values.decodeIfPresent(String.self, forKey: .cu)
        ln = try values.decodeIfPresent(String.self, forKey: .ln)
        na = try values.decodeIfPresent(String.self, forKey: .na)
        wo = try values.decodeIfPresent(String.self, forKey: .wo)
        ig = try values.decodeIfPresent(String.self, forKey: .ig)
        to = try values.decodeIfPresent(String.self, forKey: .to)
        ta = try values.decodeIfPresent(String.self, forKey: .ta)
        mt = try values.decodeIfPresent(String.self, forKey: .mt)
        ar = try values.decodeIfPresent(String.self, forKey: .ar)
        su = try values.decodeIfPresent(String.self, forKey: .su)
        ab = try values.decodeIfPresent(String.self, forKey: .ab)
        ps = try values.decodeIfPresent(String.self, forKey: .ps)
        bm = try values.decodeIfPresent(String.self, forKey: .bm)
        mi = try values.decodeIfPresent(String.self, forKey: .mi)
        kn = try values.decodeIfPresent(String.self, forKey: .kn)
        kv = try values.decodeIfPresent(String.self, forKey: .kv)
        os = try values.decodeIfPresent(String.self, forKey: .os)
        bn = try values.decodeIfPresent(String.self, forKey: .bn)
        li = try values.decodeIfPresent(String.self, forKey: .li)
        vi = try values.decodeIfPresent(String.self, forKey: .vi)
        zh = try values.decodeIfPresent(String.self, forKey: .zh)
        eo = try values.decodeIfPresent(String.self, forKey: .eo)
        ha = try values.decodeIfPresent(String.self, forKey: .ha)
        tt = try values.decodeIfPresent(String.self, forKey: .tt)
        lb = try values.decodeIfPresent(String.self, forKey: .lb)
        ce = try values.decodeIfPresent(String.self, forKey: .ce)
        hu = try values.decodeIfPresent(String.self, forKey: .hu)
        it = try values.decodeIfPresent(String.self, forKey: .it)
        tl = try values.decodeIfPresent(String.self, forKey: .tl)
        pl = try values.decodeIfPresent(String.self, forKey: .pl)
        sm = try values.decodeIfPresent(String.self, forKey: .sm)
        en = try values.decodeIfPresent(String.self, forKey: .en)
        vo = try values.decodeIfPresent(String.self, forKey: .vo)
        el = try values.decodeIfPresent(String.self, forKey: .el)
        sn = try values.decodeIfPresent(String.self, forKey: .sn)
        fr = try values.decodeIfPresent(String.self, forKey: .fr)
        cs = try values.decodeIfPresent(String.self, forKey: .cs)
        io = try values.decodeIfPresent(String.self, forKey: .io)
        hi = try values.decodeIfPresent(String.self, forKey: .hi)
        et = try values.decodeIfPresent(String.self, forKey: .et)
        pa = try values.decodeIfPresent(String.self, forKey: .pa)
        av = try values.decodeIfPresent(String.self, forKey: .av)
        ko = try values.decodeIfPresent(String.self, forKey: .ko)
        bh = try values.decodeIfPresent(String.self, forKey: .bh)
        yi = try values.decodeIfPresent(String.self, forKey: .yi)
        sa = try values.decodeIfPresent(String.self, forKey: .sa)
        sl = try values.decodeIfPresent(String.self, forKey: .sl)
        hr = try values.decodeIfPresent(String.self, forKey: .hr)
        si = try values.decodeIfPresent(String.self, forKey: .si)
        so = try values.decodeIfPresent(String.self, forKey: .so)
        gn = try values.decodeIfPresent(String.self, forKey: .gn)
        ay = try values.decodeIfPresent(String.self, forKey: .ay)
        se = try values.decodeIfPresent(String.self, forKey: .se)
        sd = try values.decodeIfPresent(String.self, forKey: .sd)
        af = try values.decodeIfPresent(String.self, forKey: .af)
        ga = try values.decodeIfPresent(String.self, forKey: .ga)
        or = try values.decodeIfPresent(String.self, forKey: .or)
        ia = try values.decodeIfPresent(String.self, forKey: .ia)
        ie = try values.decodeIfPresent(String.self, forKey: .ie)
        ug = try values.decodeIfPresent(String.self, forKey: .ug)
        nl = try values.decodeIfPresent(String.self, forKey: .nl)
        gv = try values.decodeIfPresent(String.self, forKey: .gv)
        qu = try values.decodeIfPresent(String.self, forKey: .qu)
        be = try values.decodeIfPresent(String.self, forKey: .be)
        an = try values.decodeIfPresent(String.self, forKey: .an)
        fo = try values.decodeIfPresent(String.self, forKey: .fo)
        hy = try values.decodeIfPresent(String.self, forKey: .hy)
        nv = try values.decodeIfPresent(String.self, forKey: .nv)
        bo = try values.decodeIfPresent(String.self, forKey: .bo)
        ascii = try values.decodeIfPresent(String.self, forKey: .ascii)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        lv = try values.decodeIfPresent(String.self, forKey: .lv)
        ca = try values.decodeIfPresent(String.self, forKey: .ca)
        no = try values.decodeIfPresent(String.self, forKey: .no)
        nn = try values.decodeIfPresent(String.self, forKey: .nn)
        ml = try values.decodeIfPresent(String.self, forKey: .ml)
        my = try values.decodeIfPresent(String.self, forKey: .my)
        ne = try values.decodeIfPresent(String.self, forKey: .ne)
        he = try values.decodeIfPresent(String.self, forKey: .he)
        cy = try values.decodeIfPresent(String.self, forKey: .cy)
        lo = try values.decodeIfPresent(String.self, forKey: .lo)
        jv = try values.decodeIfPresent(String.self, forKey: .jv)
        sv = try values.decodeIfPresent(String.self, forKey: .sv)
        mn = try values.decodeIfPresent(String.self, forKey: .mn)
        tg = try values.decodeIfPresent(String.self, forKey: .tg)
        kw = try values.decodeIfPresent(String.self, forKey: .kw)
        cv = try values.decodeIfPresent(String.self, forKey: .cv)
        az = try values.decodeIfPresent(String.self, forKey: .az)
        oc = try values.decodeIfPresent(String.self, forKey: .oc)
        th = try values.decodeIfPresent(String.self, forKey: .th)
        ru = try values.decodeIfPresent(String.self, forKey: .ru)
        ny = try values.decodeIfPresent(String.self, forKey: .ny)
        bs = try values.decodeIfPresent(String.self, forKey: .bs)
        st = try values.decodeIfPresent(String.self, forKey: .st)
        ro = try values.decodeIfPresent(String.self, forKey: .ro)
        rm = try values.decodeIfPresent(String.self, forKey: .rm)
        ff = try values.decodeIfPresent(String.self, forKey: .ff)
        kk = try values.decodeIfPresent(String.self, forKey: .kk)
        uk = try values.decodeIfPresent(String.self, forKey: .uk)
        pt = try values.decodeIfPresent(String.self, forKey: .pt)
        tr = try values.decodeIfPresent(String.self, forKey: .tr)
        eu = try values.decodeIfPresent(String.self, forKey: .eu)
        ht = try values.decodeIfPresent(String.self, forKey: .ht)
        ka = try values.decodeIfPresent(String.self, forKey: .ka)
        ur = try values.decodeIfPresent(String.self, forKey: .ur)
    }

}
