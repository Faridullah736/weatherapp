import Foundation

public struct CitySearch : Codable {
   public let name : String?
   public let local_names : LocalNames?
   public let lat : Double?
   public let lon : Double?
   public let country : String?
   public let state : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case local_names = "local_names"
        case lat = "lat"
        case lon = "lon"
        case country = "country"
        case state = "state"
    }

   public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        local_names = try values.decodeIfPresent(LocalNames.self, forKey: .local_names)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lon = try values.decodeIfPresent(Double.self, forKey: .lon)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        state = try values.decodeIfPresent(String.self, forKey: .state)
    }

}
