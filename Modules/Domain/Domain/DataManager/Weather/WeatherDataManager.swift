

import Foundation
import Combine

public protocol WeatherDataManagerProtocol {
    func getCurrentWeather(lat: Double, lon: Double) -> AnyPublisher<CurrentWeather, Error>
    func getForecastWeather(lat: Double, lon: Double) -> AnyPublisher<ForecastWeatherResponse, Error>
    func getCityWeather(param: String) -> AnyPublisher<[CitySearch], Error>
}

public class WeatherDataManager: WeatherDataManagerProtocol {

    private let remoteDataSource : WeatherRemoteRepository
    private let localDataSource: WeatherLocalRepository

    public init(remoteDataSource: WeatherRemoteRepository,
                localRepository: WeatherLocalRepository) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localRepository
    }

    public func getCurrentWeather(lat: Double, lon: Double) -> AnyPublisher<CurrentWeather, Error>  {
        remoteDataSource.getCurrentWeather(lat: lat, lon: lon).eraseToAnyPublisher()
    }

    public func getForecastWeather(lat: Double, lon: Double) -> AnyPublisher<ForecastWeatherResponse, Error> {
        remoteDataSource.getForecastWeather(lat: lat, lon: lon).eraseToAnyPublisher()
    }
    
    public func getCityWeather(param: String) -> AnyPublisher<[CitySearch], Error> {
        remoteDataSource.getCityWeather(param: param).eraseToAnyPublisher()
    }
}
