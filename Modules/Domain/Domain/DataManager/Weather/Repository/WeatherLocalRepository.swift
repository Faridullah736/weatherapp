
import Foundation

public protocol WeatherLocalRepository {
    var currentWeather: CurrentWeather { get set}
    var forecastWeather: ForecastWeatherResponse { get set }
}
