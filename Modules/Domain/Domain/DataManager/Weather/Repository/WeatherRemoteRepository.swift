
import Foundation
import Combine

public protocol WeatherRemoteRepository {
    func getCurrentWeather(lat: Double, lon: Double) -> AnyPublisher<CurrentWeather, Error>
    func getForecastWeather(lat: Double, lon: Double) -> AnyPublisher<ForecastWeatherResponse, Error>
    func getCityWeather(param: String) -> AnyPublisher<[CitySearch], Error>
}
